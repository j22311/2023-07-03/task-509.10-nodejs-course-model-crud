const getReviewAllMiddleware =  (req,res,next) =>{
    console.log("Review -> get All review");
    next();
}

const getReviewAMiddleware =  (req,res,next) =>{
    console.log("Review -> get A review");
    next();
}

const postAReviewMiddleware =  (req,res,next) =>{
    console.log("Review -> creata A review");
    next();
}

const putAReviewMiddleware =  (req,res,next) =>{
    console.log("review -> Update All review");
    next();
}

const deleteAReviewMiddleware =  (req,res,next) =>{
    console.log("Review -> delete A review");
    next();
}

module.exports = {
    deleteAReviewMiddleware,
    putAReviewMiddleware,
    postAReviewMiddleware,
    getReviewAMiddleware,
    getReviewAllMiddleware
}
const getAllcourseMIddleware =  (req,res,next) =>{
    console.log("get All (in middleware) Courses");
    next();
}

const getAcourseMIddleware =  (req,res,next) =>{
    console.log("(in middleware) get A Courses");
    next();
}

const postAcourseMIddleware =  (req,res,next) =>{
    console.log(" (in middleware) creata A Courses");
    next();
}

const putAcourseMIddleware =  (req,res,next) =>{
    console.log(" (in middleware)Update All Courses");
    next();
}

const deleteAcourseMIddleware =  (req,res,next) =>{
    console.log("(in middleware) delete A Courses");
    next();
}

module.exports = {
    deleteAcourseMIddleware,
    putAcourseMIddleware,
    postAcourseMIddleware,
    getAcourseMIddleware,
    getAllcourseMIddleware
}
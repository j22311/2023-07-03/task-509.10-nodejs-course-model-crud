// khai báo thư viện 
const express = require("express");

//khai báo hoặc import reviewmiddleware

 const {
    deleteAReviewMiddleware,
    putAReviewMiddleware,
    postAReviewMiddleware,
    getReviewAMiddleware,
    getReviewAllMiddleware
 } = require ('../middlewares/reviewMiddleware');

 //tạo review Rounter
 const reviewRounter = express.Router();

 //sử dụng rounter cho các Review
 reviewRounter.get("/review",getReviewAllMiddleware, (req,res) => {
    res.json({
        message: "Get all review"
    });
})

reviewRounter.get("/review/:reviewId",getReviewAMiddleware, (req,res) => {
   let reviewID = req.params.reviewId;
    res.json({
        message: `get a reivew ID = ${reviewID}`
    });
})

reviewRounter.post("/review/",postAReviewMiddleware, (req,res) => {
    
     res.json({
         message: `create a reivew `
     });
 })

 reviewRounter.put("/review/:reviewId",putAReviewMiddleware, (req,res) => {
    let reviewID = req.params.reviewId;
     res.json({
         message: `update a reivew ID = ${reviewID}`
     });
 })
 
 reviewRounter.delete("/review/:reviewId",deleteAReviewMiddleware, (req,res) => {
    let reviewID = req.params.reviewId;
     res.json({
         message: `delete a reivew ID = ${reviewID}`
     });
 })
module.exports = {
    reviewRounter
}